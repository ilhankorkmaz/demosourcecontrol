//
//  ViewController.swift
//  demosourcecontrol
//
//  Created by ilhan korkmaz on 2.10.2018.
//  Copyright © 2018 ilhan korkmaz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var button: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    private func sayHello(){
        print("Hello")
    }
    
    func doSomething(){
        print("Doing something")
    }
}

